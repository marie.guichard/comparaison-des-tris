from timeit import timeit
import matplotlib.pyplot as plt
from tris_liste import *
NBRE_ESSAIS = 100

def cree_liste(n,ordre):
    if str(ordre) == 'melangee':
        return cree_liste_melangee(n)
    elif str(ordre) == 'croissant':
        return cree_liste_croissante(n)
    else:
        return cree_liste_decroissante(n)
    
   
    

def temps_tris(tris,n,ordre):
    liste = cree_liste(n,ordre)
    if tris != 'sort':
        temps = timeit(setup='from analyse_tris import '+tris+' ; from tris_liste import cree_liste_melangee',
               stmt= str(tris)+'('+str(liste)+')',number=100)
        return temps
    else:
        temps = timeit(setup = 'from tris_liste import cree_liste_melangee',
                   stmt = str(liste)+'.sort()', number = 100)
        return temps
   
    

    
def temps_tris_plusieurs_essais(tri_choisi, n,ordre):   
    resultat = []
    abscisse = [i for i in range(n)]
    for i in range (n):
        temps = temps_tris(tri_choisi,i,ordre)
        resultat.append(temps)
    plt.plot(abscisse,resultat)
    plt.title('comparaison des différents tris')
    plt.xlabel('taille des listes')
    plt.ylabel('temps en secondes')
    plt.grid()
    plt.savefig(str(tri_choisi)+str(ordre)+str(n)+'.png', transparent = True)
    plt.show()
    plt.close()

def comparaison_tris(n,ordre):
    resultat=[[0 for i in range(n)] for j in range(5)]
    abscisse = [i for i in range(n)]
    tris = ['tri_fusion','tri_rapide','tri_select','tri_insert','sort']
    for j in range(5):
        for i in range(n):
            temps = temps_tris(tris[j],i,ordre)
            resultat[j][i]= temps
        plt.plot(abscisse, resultat[j])
    plt.legend(('tri fusion','tri rapide','tri selection','tri insertion','sort'), loc = 'upper left')
    plt.title('comparaison des différents tris')
    plt.xlabel('taille des listes')
    plt.ylabel('temps en secondes')
    plt.grid()
    plt.savefig('4 tris'+str(ordre)+str(n)+'.png', transparent = True)
    plt.show()
    plt.close()



    

    
    

        
